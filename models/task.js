var util = require('util');
var async = require('async');

var mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    task_name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    text: {
        type: String
    },
    share: {
        type: Number
    }
});

schema.statics.createTask = function(task_name, username, share, text) {

    var Task = this;
    var task = new Task({task_name: task_name, username: username, text:text, share:share});
    task.save(function() {
        return false;
    });
};

schema.statics.updateTask = function(task_id, share, text) {

    var Task = this;
    Task.findByIdAndUpdate(task_id, { $set: { text: text, share:share }}, function (err, task) {
        if (err) return err;
        return task;
    });

};

schema.statics.deleteTask = function(id) {

    console.log(id);
    var Task = this;
    Task.findByIdAndRemove(id, function(err, status){
        if (err) return handleError(err);
        return status;
    });

};
exports.Task = mongoose.model('Task', schema);
