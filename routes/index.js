var checkAuth = require('../middleware/checkAuth');

module.exports = function(app) {

  app.get('/', require('./frontpage').get);

  app.get('/login', require('./login').get);
  app.post('/login', require('./login').post);

  app.post('/logout', require('./logout').post);

  app.get('/task_create', require('./task_create').get);
  app.post('/task_create', require('./task_create').post);

  app.get('/task_list', require('./task_list').get);

  app.get('/task_edit', require('./task_edit').get);
  app.post('/task_edit', require('./task_edit').post);

  app.get('/task_delete', require('./task_delete').get);


};
