var Task = require('../models/task').Task;
url = require("url");
qs = require("querystring");

exports.get = function(req, res) {
    var query = url.parse(req.url).query,
        params = qs.parse(query);
    var id = params['id'];
    if (id){
        var task_body = Task.findById(id, function(err, task_body){
            res.render('task_edit', {
                locals: {
                    id:id,
                    task_body : task_body
                }
            });
        });
    } else {
        return false;
    }
};

exports.post = function(req, res, next) {
    var id = req.body.task_id;
    var text = req.body.text;
    var share = req.body.share;
    console.log(id);
    console.log(share);
    console.log(text);
    Task.updateTask(id, share, text);
    var task_body = Task.findById(id, function(err, task_body){
        res.render('task_edit', {
            locals: {
                id:id,
                task_body : task_body
            }
        });
    });
};