var Task = require('../models/task').Task;


exports.get = function(req, res) {
  res.render('task_create');
};

exports.post = function(req, res, next) {
  var username = req.body.username;
  var task_name = req.body.task_name;
  var share = req.body.share;
  var text = req.body.text;
  console.log(username);
  console.log(task_name);
  console.log(share);
  console.log(text);
  Task.createTask(task_name, username, share, text);
  res.render('task_list');
};