var Task = require('../models/task').Task;
url = require("url");
qs = require("querystring");

exports.get = function(req, res) {
    var query = url.parse(req.url).query,
        params = qs.parse(query);
    var id = params['id'];
    Task.deleteTask(id);
    var username = req.session.username;
    if (username){
        var shared_tasks = Task.find({share: 1}, function(err, task){
            shared_tasks = task;
        });
        var tasks = Task.find({username: username}, function(err, tasks){
            res.render('task_list', {
                locals: {
                    tasks:tasks,
                    shared_tasks:shared_tasks
                }
            });
        });
    } else {
        return false;
    }
};